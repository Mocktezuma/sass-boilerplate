import os
import shutil


sass = os.getcwd()+'/sass'  # current directory + /sass

# File name
abstracts = {1: 'variables', 2: 'functions', 3: 'mixins', 4: 'placeholders'}
base = {1: 'reset', 2: 'typography'}
components = {1: 'buttons', 2: 'carousel', 3: 'cover', 4: 'dropdown'}
layout = {1: 'navigation', 2: 'grid', 3: 'header',
          4: 'footer', 5: 'sidebar', 6: 'forms'}
pages = {1: 'home', 2: 'contact'}
themes = {}
vendors = {}

# folder structure
sassFolders = {'base': base, 'abstracts': abstracts, 'components': components, 'layout':
               layout, 'pages': pages, 'themes': themes, 'vendors': vendors}

element = []


def addElement(category, element):
    u = len(category)
    category[u+1] = element

    return category


def foldStruct():
    i = 1
    for x in sassFolders:
        os.makedirs(sass+"/"+x)     # Create folder structure
        y = len(sassFolders[x])
        while i <= y:
            # Create files
            open(sass+"/"+x+"/"+"_"+sassFolders[x][i]+".scss", 'a').close()
            i = i + 1
        i = 1


def createScss():
    f = open(sass+"/main.scss", 'w+')
    i = 1
    for x in sassFolders:  # write main.scss file with @import
        o = len(sassFolders[x])
        while i <= o:
            f.write("@import \""+x+"/"+sassFolders[x][i]+"\";\n")
            i = i + 1
        f.write("\n")  # add spaces between categories
        i = 1


z = 0
for i in sassFolders:

    print("those files are going to be written inside "+i+" :")
    print(sassFolders[i])

if os.path.isdir(sass):
    shutil.rmtree(sass, ignore_errors=False, onerror=None)
    foldStruct()
else:
    foldStruct()

createScss()
